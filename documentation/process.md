# Development Process
<br>

### Recommended Steps
0. Cloning the repository - (only needed first time)<br><br>
1. Pulling new changes
2. Check existing issues
3. Create a new branch
4. Source Control

<br>
<br>

## Cloning the Repository
Cloning the repo creates copies a local version of the code onto your machine where you can freely make changes, unaffected by other contributors.<br>
1. Navigate into directory you would like to clone the project into (using the command: `cd subdirectory-name`)
    - ie: By preference, the project exists on my Desktop: (C:\Users\gbadu\OneDrive\Desktop\alpfa-unm)
2. At the top of the page of the alpfa-unm repo you will find a box that says SSH to the left, what looks like a URL to the right, beginning with 'git@...". Copy this string.
3. Back in the terminal, paste this into the command `git clone <paste-url-here>`

<br>
<br>

## Pulling new changes
When starting work, it is best practice to integrate changes other contributors have implemented since the last time you worked. This way you are working with the newest version of the code. 
<br>

The `git fetch` command downloads data from remote repos (others branches)
<br>

The `git pull` command command downloads changes and directly integrates others' changes into your local repo.

<br>
<br>

## Check existing issues
The 'Issues' menu can be found in the left sidebar in Gitlab. Items of work/improvements needing attention can be documented here. While Gitlab allows a particular contributer to be the "assignee" of an issue, most will be left open to anyone who would like to take it. To avoid

<br>
<br>

## Create a new branch
By creating your own branch, you may freely implement changes without interupting others or altering what is already in the `dev` or `master`. Each contributor should always create a new branch when implementing changes so that a "Gold Standard" of code is maintained. The command to create a new branch is:
```
git checkout -b my-new-branch-name
```
The `-b` tag indicates creation of a new branch. To switch to an existing branch simply use: `git checkout <branch-name>`


<br>
<br>

## Source Control
As explained previously, cloning the repo copies the project locally on your machine. The commands below demonstrate how to commit and push code back up to Gitlab.

<br>

```
git status
```
The 'git status' command displays all the files that have been altered locally. The changes appear in red text indicating they have not been added.

<br>
<br>

```
'git add --all'     or     'git add <file-name.ext>
```
The 'git add' command stages all (or specified) changes for commit. If you run another `git status` you will observe the changes now appear in green.

<br>
<br>

```
git commit -m "explanation of changes"
```
The 'git commit' command saves the added changes to your *local* repository.

<br>
<br>

```
git push
```
The 'git push' command pushes your local repo up to Gitlab. If pushing to a new branch for the first time, you will be prompted to use:<br>`git push --set-upstream origin <branch-name>`.

<br>
<br>

The benefits of following the git flow include:
- Maintaining "benchmark" branches to revert to (dev, master)
- Allowing multiple contributers to work on a project without interfering with one another
- Maintain code on somewhere other than just your local machine in the case of machine failure
- Project is accessible anywhere and to anyone with a Gitlab account

<br>
<br>
<br>

## [Merge Requests](./documentation/merge-request.md)