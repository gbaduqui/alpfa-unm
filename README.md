# ALPFA UNM website repo
> Help on how to use Git and contribute to the webpage

<br>

## Purpose
In addition to promoting ALPFA UNM across campus, this project serves as a way to mimic working in a real-world web development environment. The project contains intro-level technologies to serve as a learning opportunity for students of any background. The 4 files below provide detailed documentation on how to get setup and contribute using Gitlab.

<br>
<br>

## How to contribute
- [Getting Started](./documentation/pre-reqs.md)
- [Configuring SSH](./documentation/ssh-config.md)
- [Development Process](./documentation/process.md)
- [Merge Requests](./documentation/merge-request.md)

<br>
<br>

## Current Technology (Fall 2018)
As of October 2018, the project contains basic front-end technologies (html, css, js, bootstrap). The mailchimp subscirber-adding service is currently being implemented in PHP.
<br>
<br>
[This video series](https://youtu.be/O9Uauq-Gd0c) on YouTube may be helpful for students with little-to-no experience with basic front-end development who are intersted in contributing.
<br>
<br>
UNM IT currently only supports PHP (....I know...sigh...) and MySQL on the back-end for custom web applications. I highly encourage future ALPFA UNM chapters to exlpore newer tech as UNM IT makes them available.

<br>
<br>

#### File Structure
```
|-- _.gitlab
   +-- merge-request-template.md
|-- _assets
   +-- css/
   +-- data/
   +-- images/  
   +-- js/
   +-- alpfa-icon.ico
|-- documentation
   +-- merge-request.md
   +-- pre-reqs.md
   +-- process.md
   +-- ssh-config.md
|-- pages
   +-- about.html
   +-- events.html
   +-- gallery.html
   +-- members.html
   +-- sponsors.html
   +-- subscribe.html
   +-- success.html
|-- addNewEmail.php
|-- index.html
|-- README.html
```