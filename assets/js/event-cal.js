$(document).ready( () => {
    $('#calendar').fullCalendar({

        //  Documentation available at: //
        // https://fullcalendar.io/docs //

        height: 40,
        themeSystem: 'bootstrap4',
        header: {
            right: 'prev, next'
        },        
        weekends: false,
        eventColor: '#558fed',

        //Events Array
        events: [
            //Event 1
            {
                title: 'Monthly Chapter Meeting',
                start: '2019-01-25T12:00:00',
                end:   '2019-01-25T13:30:00',
                allDay: false
            },
            //Event 2
            {
                title: 'Student Socail',
                start: '2019-02-02T18:00:00',
                end:   '2019-02-02T20:00:00',
                allDay: false
            },
            //Event 3
            {
                title: 'Mentorship Kickoff',
                start: '2019-02-07T17:30:00',
                end:   '2019-02-07T19:00:00',
                allDay: false
            },
            //Event 4
            {
                title: 'Monthly Chapter Meeting',
                start: '2019-02-22T12:00:00',
                end:   '2019-02-22T13:00:00',
                allDay: false
            },
            //Event 5
            {
                title: 'Community Service',
                start: '2019-02-23T11:00:00',
                end:   '2019-02-23T12:30:00',
                allDay: false
            },
            //Event 6
            {
                title: 'Bowl-a-thon Fundraiser',
                start: '2019-03-03T17:00:00',
                end:   '2019-03-03T19:30:00',
                allDay: false
            },
            //Event 7
            {
                title: 'Monthly Chapter Meeting',
                start: '2019-04-22T12:00:00',
                end:   '2019-04-22T13:30:00',
                allDay: false
            },
            //Event 8
            {
                title: 'Community Service',
                start: '2019-04-01T11:00:00',
                end:   '2019-04-01T12:30:00',
                allDay: false
            },
            //Event 9
            {
                title: 'Monthly Chapter Meeting',
                start: '2019-04-19T12:00:00',
                end:   '2019-04-19T13:30:00',
                allDay: false
            },
            //Event 10
            {
                title: 'Student Social',
                start: '2019-04-19T18:00:00',
                end:   '2019-04-19T20:00:00',
                allDay: false
            },
            //Event 11
            {
                title: 'Mentorship Wrap Up',
                start: '2019-05-01T17:30:00',
                end:   '2019-05-01T19:00:00',
                allDay: false
            }
        ]
    });
});