//Date variables
//-----These variables are used to render the next event in the index and events pages
var today = new Date();
var month = today.getMonth() + 1;
var date = today.getDate();
var days = today.getFullYear() % 4 == 0 ? 366 : 365;


//Universal image route
//-----This constant is concatenated with the image value to provide each image's relative path
var imageSource = './assets/images/events/';

//Events Array
//-----This constant loads the object in the events.json file to be accessed
var springEvents = [
    //event1
    {
        title: "Monthly Chapter Meeting",
        month: 1,
        day: 25,
        image: imageSource + 'orientation.jpg'
    },
    //event2
    {
        title: "Student Social",
        month: 2,
        day: 2,
        image: imageSource + 'studentSocial.jpg'
    },
    //event3
    {
        title: "Mentorship Kickoff",
        month: 2,
        day: 7,
        image: imageSource + 'sandbarLogo.png'
    },
    //event4
    {
        title: "Monthly Chapter Meeting",
        month: 2,
        day: 22,
        image: imageSource + 'Pepsi.png'
    },
    //event5
    {
        title: "Community Service",
        month: 2,
        day: 23,
        image: imageSource + 'Storehouse.jpg'
    },
    //event 6
    {
        title: "ALPFA UNM Fundraiser",
        month: 3,
        day: 2,
        image: imageSource + 'BowlingLane.jpg'
    },

    //event7

    {
        title: "Monthly Chapter Meeting",
        month: 3,
        day: 22,
        image: imageSource + 'PWC.png'
    },
    //event8
    {
        title: "Community Service",
        month: 4,
        day: 1,
        image: imageSource + 'fallFrenzy.jpg'
    },
    //event9
    {
        title: "Monthly Chapter Meeting",
        month: 4,
        day: 19,
        image: imageSource + 'Honeywell.jpg'
    },
    //event 10
    {
        title: "Student Social",
        month: 4,
        day: 19,
        image: imageSource + 'studentSocial.jpg'
    },
    //event11
    {
        title: "Mentorship Wrap Up",
        month: 5,
        day: 1,
        image: imageSource + 'meetGreet.jpg'
    }
];// End events array


// ----- //
findNextEvent();
// ----- //


//Function to set next event
function findNextEvent() {
    i = 0;
    eventFound = false;

    while ((i < springEvents.length) && (eventFound === false)) {
        
        if ((springEvents[i].month === month) && (springEvents[i].day >= date)) {
            document.getElementById('iTitle').innerHTML = springEvents[i].title;
            document.getElementById('iImage').src = springEvents[i].image;

            eventFound = true;
        }
        
        i++;

    }// End while loop
}// End findNextEvent() function


////////////////////////////////////////////////////////////////////////////
//////////// Render motivational quote randomly ////////////////////////////                
////////////////////////////////////////////////////////////////////////////

// Daily Motivation quote array //
var quotes = [
    '"Success is not final; failure is not fatal: It is the courage to continue that counts."',
    '"It is better to fail in originality than to succeed in imitation."',
    '"Success usually comes to those who are too busy to be looking for it."',
    '"Opportunities don\'t happen. You create them."',
    '"Don\'t be afraid to give up the good to go for the great."',
    '"I find that the harder I work, the more luck I seem to have."',
    '"There are two types of people who will tell you that you cannot make a difference in this world: those who are afraid to try and those who are afraid you will succeed."',
    '"Successful people do what unsuccessful people are not willing to do. Don\'t wish it were easier; wish you were better."',
    '"Success is walking from failure to failure with no loss of enthusiasm."',
    '"If you are not willing to risk the usual, you will have to settle for the ordinary."',
    '"The ones who are crazy enough to think they can change the world, are the ones that do."',
    '"Do one thing every day that scares you."',
    '"All progress takes place outside the comfort zone."',
    '"Don\'t let the fear of losing be greater than the excitement of winning."',
    '"The only limit to our realization of tomorrow will be our doubts of today."',
    '"Character cannot be developed in ease and quiet. Only through experience of trial and suffering can the soul be strengthened, ambition inspired, and success achieved."',
    '"The way to get started is to quit talking and begin doing."',
    '"There are no secrets to success. It is the result of preparation, hard work, and learning from failure."',
    '"Success seems to be connected with action. Successful people keep moving. They make mistakes, but they don\'t quit."',
    '"If you really want to do something, you\'ll find a way. If you don\'t, you\'ll find an excuse."',
    '"Success isn\'t just about what you accomplish in your life; it\'s about what you inspire others to do."',
    '"Some people dream of success while others wake up and work."',
    '"If you can dream it, you can do it."',
    '"The difference between who you are and who you want to be is what you do."',
    '"A successful individual is one who can lay a firm foundation with the bricks that other throw at them."',
    '"In order to succeed, we must first believe that we can."',
    '"Many of life\'s failures are people who did not realize how close they were to success when they gave up."',
    '"You know you are on the road to success if you would do your job, and not be paid for it."',
    '"I failed my way to success."',
    '"I never dreamed about success, I worked for it."',
    '"I never did anything worth doing by accident, nor did any of my inventions come indirectly through accident ... when I have fully decided that a result is worth getting, I go about it, and make trial after trial, until it comes."',
    '"The only place where success comes before work is in the dictionary."',
    '"Keep on going, and the chances are that you will stumble on something, perhaps when you are least expecting it. I never heard of anyone ever stumbling on something after quitting"',
    '"The secret of change is to focus all your energy not on fighting the old but on building the new."',
    '"Always deliver more than expected."',
    '"If you can push through that feeling of being scared, that feeling of taking a risk, really amazing things can happen."',
    '"If people like you, they’ll listen to you, but if they trust you, they’ll do business with you."',
    '"It’s not about ideas. It’s about making ideas happen."',
    '"The fastest way to change yourself is to hang out with people who are already the way you want to be."',
    '"There are no secrets to success. It is the result of preparation, hard work, and learning from failure."',
    '"Don’t worry about failure; you only have to be right once."',
    '"People who succeed have momentum. The more they succeed, the more they want to succeed, and the more they find a way to succeed."',
    '"Character cannot be developed in ease and quiet. Only through experience of trial and suffering can the soul be strengthened, ambition inspired, and success achieved."',
    '"Don’t worry about people stealing your ideas. Worry about the day they stop."',
    '"However difficult life may seem, there is always something you can do and succeed at."',
    '"The last 10% it takes to launch something takes as much energy as the first 90%."',
    '"The real test is not whether you avoid this failure, because you won’t. It’s whether you let it harden or shame you into inaction, or whether you learn from it; whether you choose to persevere."',
    '"Ideas are easy. Implementation is hard."',
    '"You gain strength, courage, and confidence by every experience in which you really stop to look fear in the face. You are able to say to yourself, I lived through this horror. I can take the next thing that comes along."',
    '"See things in the present, even if they are in the future."',
    '"Success is the sum of small efforts – repeated day in and day out."',
    '"I knew that if I failed I wouldn’t regret that, but I knew the one thing I might regret is not trying."',
    '"Whether you think you can, or think you can’t – you’re right."',
    '"Step out of the history that is holding you back. Step into the new story you are willing to create."',
    '"Don’t be cocky. Don’t be flashy. There’s always someone better than you."',
    '"Embrace what you don’t know, especially in the beginning, because what you don’t know can become your greatest asset. It ensures that you will absolutely be doing things different from everybody else."',
    '"Risk more than others think is safe. Dream more than others think is practical."',
    'A person who is quietly confident makes the best leader."',
    '"A goal without a plan is a wish"', 
    // Gabe Baduqui, Dir. Membership 18-19
];

if(days <= quotes.length)
{
    document.getElementById('motivation').innerHTML = quotes[days];
}

else
{
    document.getElementById('motivation').innerHTML = quotes[days % quotes.length];
};
