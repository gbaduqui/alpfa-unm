var imgPath = '../assets/images/about/';

var officers = [
    // President
    {
        name: 'Deisy Ruiz',
        image: imgPath + 'deisy.jpg',
        title: 'President',
        bio: 'Deisy is graduate student pursuing a MAACT. Deisy has ' +
        'been an intern at Sandia National Labs for two in a ' +
        'half years and completed a summer internship with PwC. ' +
        'Deisy joined ALPFA because she loved the idea of ' +
        'empowering other Latinos, like herself being a ' +
        'first-generation student. For Deisy, ALPFA means more ' +
        'than just a business organization. She believes ALPFA ' +
        'is the gateway of helping, growing, and building the ' +
        'success of future business leaders in the Latino communities ' +
        'and more. Deisy plans to continue growing the chapter but ' +
        'still keeping the core values of ALPFA.'
    },
    // Vice President
    {
        name: 'David Rodriguez',
        image: imgPath + 'david.jpg',
        title: 'Vice President',
        bio: 'David is a first year Law school student. He graduated with a Bachelor\'s' +
        ' in finance. He has been a member of ALPFA since August 2017. David has become' +
        ' passionate about ALPFA. He strongly believes in the values of ALPFA that he always' +
        ' helps other students in any way he can, that involves with networking & professionalism.'
    },
    // Treasurer
    {
        name: 'Devin Salazar',
        image: imgPath + 'devin.jpg',
        title: 'Treasurer',
        bio: 'Devin is a fourth-year undergraduate student pursuing a degree in ' +
        'Accounting. Devin just recently started an internship at ' +
        'Sandia National Laborataories through ALPFA. She was mentored ' +
        'from a fellow ALPFA officer in helping her decide her major, ' +
        'which led her to join the organization. For Devin, she has ' +
        'received so many great opportunities through ALPFA that she ' +
        'wants others to experience what she did. Devin has become extremely ' +
        'passionate about ALPFA and hopes to grow the chapter and help ' +
        'empower other Latino leaders like herself. '
    },
    // Secretary
    {
        name: 'Justin Martinez',
        image: imgPath + 'justin.jpg',
        title: 'Secretary',
        bio: 'Justin is a fourth-year undergraduate pursuing a degree in ' +
        'Human Resource Management. Justin has been an intern at Sandia ' +
        'National Laboratories for a year working within Sandia’s Talent ' +
        'Acquisition Center. Justin joined ALPFA because he loves creating ' +
        'lasting friendships and helping to empower future leaders at the ' +
        'University of New Mexico. To him, ALPFA is an opportunity to grow ' +
        'and take steps toward becoming a true professional.'
    },
    // Dir. Multi-Media
    {
        name: 'Jerome Mendoza',
        image: imgPath + 'jerome.jpg',
        title: 'Director of Multi-Media',
        bio: 'Jerome is a graduate student pursuing a MACCT. He has been an intern ' +
        'at Sandia National Labs for over a year in a half in the procurement ' +
        'sector. Jerome was inspired to join ALPFA as he observed the ' +
        'opportunities and professional growth that his fellow class mates ' +
        'acquired. To Jerome, ALPFA offers a unique opportunity to not only ' +
        'grow as a professional but as a person through the connections he has ' +
        'made with his fellow members and the professional chapter.'
    },
    // Faculty Advisor
    {
        name: 'Tina Armijo',
        image: imgPath + 'tina.jpg',
        title: 'Faculty Advisor',
        bio: 'Tina have been at the University of New Mexico for almost 23 years and at' + 
        ' Anderson for over 11. I have helped ALPFA students in my years at Anderson' +
        ' with travel to their Annual Convention before formely becoming the Advisor in 2018.' +
        ' Tina has been instrumental to the student chapter by volunteering her time' +
        ' in chapter efforts.'
    },
    // Dir. Community
    {
        name: 'David Ibarra',
        image: imgPath + 'David_Ibarra.png',
        title: 'Director of Community Service',
        bio: 'David was born in Ciudad Juárez, but lived most of my life in Albuquerque, New' +
        ' Mexico. I am a junior in the Anderson Program for Finance and am a first-' +
        ' generation college student alongside my two older brothers, both of whom ' +
        ' graduated from UNM. I wish to obtain an MBA after completing my' +
        ' undergraduate degree and work as a public servant. The intricacies of policy-' +
        ' making and political maneuvering have always fascinated me, and ALPFA has' +
        ' helped me develop as a professional to prepare myself for this journey.'
    },
    // Dir. Events
    {
        name: 'Angelica Ramirez',
        image: imgPath + 'angie.jpg',
        title: 'Director of Events',
        bio: 'Angelica Ramirez is a fourth-year undergraduate majoring in ' +
        'Accounting. She interned at Sandia National Labotories where ' +
        'she worked in project management. Angelica was a member of the ' +
        'University of Arizona ALPFA chapter, and was encouraged by a ' +
        'fellow officer to join the UNM ALPFA chapter. Angelica is ' +
        'thrilled with the opportunity to grow and develop within ALPFA ' +
        'while empowering other Latino leaders.'
    },
    // Dir of Mentorship
    {
        name: 'Brittney Vasquez',
        image: imgPath + 'brittney.jpg',
        title: 'Director of Mentorship',
        bio: 'Brittney Vasquez is a UNM Anderson student in her ' +
        'senior year working to obtain her BBA with a concentration in Accounting. ' +
        'After serving active duty in the Air Force for three years she returned ' +
        'to her home state to complete her degree while serving in the NM Air ' +
        'National Guard. Through a UNM career event Brittney, was eventually selected for the ' +
        '2018 PricewaterhouseCoopers Elevate Summer Leadership Program in Denver. And has ' +
        'recently accepted a summer internship position in the PwC Denver office. ' +
        'Brittney has recently taken on the ALPFA Mentorship ' +
        'Director role and is very passionate about continuous growth and helping ' +
        'others enhance their networking skills and professional careers.'
    },

];

output = '';

for(i = 0; i < officers.length; i++) {
    output += '<div class="row"><div class="col-sm-6"><h2>' +
        officers[i].name + '</h2><img src="' + officers[i].image +
        '" alt="" width="50%"><h3>' + officers[i].title + 
        '</h3></div><br><br><div class="col-sm-6">' +
        '<p>' + officers[i].bio + '</p></div></div><br><br>';
}

document.getElementById('studentboard').innerHTML = output;