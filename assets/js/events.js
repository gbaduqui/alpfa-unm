//Date variables
//-----These variables are used to render the next event in the index and events pages
var today = new Date();
var month = today.getMonth() + 1;
var date = today.getDate();


//Universal image route
//-----This constant is concatenated with the image value to provide each image's relative path
var imageSource = '../assets/images/events/';

//Events Objects Array
//-----This constant loads the object in the events.json file to be accessed
var springEvents = [
    //event1
    {
        title: "ALPFA Orientation",
        month: 8,
        monthStr: 'August',
        day: 28,
        time: "12:00pm",
        location: "Virtual",
        dress: "Business Casual",
        description: "Learn more about the organization's mission and the benefits of joining the ALPFAmilia.",
        image: imageSource + 'orientation.jpg'
    },
    //event2
    {
        title: "Student Social",
        month: 9,
        monthStr: 'September',
        day: 3,
        time: "5:30pm",
        location: "Virtual",
        dress: "Casual",
        description: "Relax and socialize with fellow chapter members!",
        image: imageSource + 'studentSocial.jpg'
    },
    //event3
    {
        title: "Student Workshop",
        month: 9,
        monthStr: 'September',
        day: 14,
        time: "TBD",
        location: "Virtual",
        dress: "Business Casual",
        description: "Workshop Details",
        image: imageSource + 'workshop.jpg'
    },
    //event4
    {
        title: "Chapter Meeting - Sandia National Laboratories",
        month: 9,
        monthStr: 'September',
        day: 18,
        time: "12:00pm",
        location: "Virtual",
        dress: "Business Casual",
        description: "Sandia National Laboratories",
        image: imageSource + 'sandia.png'
    },
    //event5
    {
        title: "Virtual Alumni Social",
        month: 10,
        monthStr: 'October',
        day: 8,
        time: "6:00pm",
        location: "Virtual",
        dress: "Casual",
        description: "Virtual Alumni Social",
        image: imageSource + 'meetGreet.jpg'
    },
    //event6
    {
        title: "Chapter Meeting - KPMG",
        month: 10,
        monthStr: 'October',
        day: 23,
        time: "12:00pm",
        location: "Virtual",
        dress: "Business Casual",
        description: "Chapter Meeting - KPMG",
        image: imageSource + 'kpmg.jpg'
    },
    //event7
    {
        title: "Student Social",
        month: 11,
        monthStr: 'November',
        day: 12,
        time: "5:30pm",
        location: "Virtual",
        dress: "Casual",
        description: "Student Social",
        image: imageSource + 'sandbarLogo.png'
    },
    //event8
    {
        title: "Chapter Meeting - E&J Gallo",
        month: 11,
        monthStr: 'November',
        day: 20,
        time: "12:00pm",
        location: "Virtual",
        dress: "Casual",
        description: "Chapter Meeting - E&J Gallo",
        image: imageSource + 'ejgallo.jfif'
    },
    //event9
    {
        title: "End of Semester Celebration",
        month: 12,
        monthStr: 'December',
        day: 18,
        time: "6:00pm",
        location: "Virtual",
        dress: "Casual",
        description: "End of Semester Celebration",
        image: imageSource + 'studentSocial.jpg'
    }
];// End events array


// ----- //
findNextEvent();

pasteHTML();
// ----- //


//Function to set next event
function findNextEvent() {
    i = 0;
    eventFound = false;

    while ((i < springEvents.length) && (eventFound === false)) {

        if ((springEvents[i].month === month) && (springEvents[i].day >= date)) {
            document.getElementById('cardTitle').innerHTML = springEvents[i].title;
            document.getElementById('cardDate').innerHTML = springEvents[i].monthStr + ' ' + springEvents[i].day;
            document.getElementById('cardTime').innerHTML = springEvents[i].time;
            document.getElementById('cardLocation').innerHTML = springEvents[i].location;
            document.getElementById('cardDress').innerHTML = springEvents[i].dress;
            document.getElementById('cardDesc').innerHTML = springEvents[i].description;
            document.getElementById('cardImg').src = springEvents[i].image;

            eventFound = true;
        }

        i++;

    }// End while loop
}// End findNextEvent() function


// Function to print event HTML //
function pasteHTML() {
    var output = '';

    for (i = 0; i < springEvents.length; i++) {
        output += '<div class="row"><div class="col-sm-6"><h2 id="eventTitle' + [i] + '"></h2><div class="eventLeft"><h3>Date: <span id="eventDate' + [i] + '"></span></h3><br><h3>Location: <span id="eventLocation' + [i] + '"></span></h3><br><h3>Dress: <span id="eventDress' + [i] +'"></span></h3><br><p id="eventDesc' + [i] +'"></p></div></div><div class="col-sm-6"><img src="" alt="" id="eventImg' + [i] +'"></div></div><br><br>';
    }

    document.getElementById('spring-events').innerHTML = output;

    for (i = 0; i < springEvents.length; i++) {
        document.getElementById('eventTitle' + [i]).innerHTML = springEvents[i].title;
        document.getElementById('eventDate' + [i]).innerHTML = springEvents[i].monthStr + ' ' + springEvents[i].day;
        document.getElementById('eventLocation' + [i]).innerHTML = springEvents[i].location;
        document.getElementById('eventDress' + [i]).innerHTML = springEvents[i].dress;
        document.getElementById('eventDesc' + [i]).innerHTML = springEvents[i].description;
        document.getElementById('eventImg' + [i]).src = springEvents[i].image;
    }
}// End pasteHTML() function
